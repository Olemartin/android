package com.example.olema.parsejson;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private TextView jsonData;
    private ProgressBar spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Importing button from the layout
        Button buttonHit = (Button) findViewById(R.id.buttonHit);
        //Importing progressbar from the layout
        spinner = (ProgressBar)findViewById(R.id.progressBar1);
        //Set the progress bar to not show at start up
        //Use spinner.setVisibility(View.VISIBLE); to show at start up
        spinner.setVisibility(View.GONE);
        //Importing the TextView where the joke is going to be
        jsonData = (TextView) findViewById(R.id.JsonItem);

        //On click listner that waits for someone to click the big button
        buttonHit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Once the button is cliced, the progressbar is shown
                spinner.setVisibility(View.VISIBLE);

                //Start the JSONTASK below, which retrieves and parses the data
                new JSONTask().execute("http://api.icndb.com/jokes/random/");

                //Make the progressbar disappear. A slight problem is that the amount of time it
                //takes for the data to be retrieved and parsed is minimal, so you wont be able
                //to see the progressbar at all...
                spinner.setVisibility(View.GONE);
            }
        });

    }


    public class JSONTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            //Initilize
            HttpURLConnection connection = null;
            BufferedReader reader = null;



            try {
                //Get the URL from the parameter which receives a string from the onClickListner
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                //Connect to the website
                connection.connect();
                //Download the content
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));

                //Put the content in a buffer
                StringBuffer buffer = new StringBuffer();
                String line = "";
                //As long as it's not null, the buffer will receive the data.
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                //Convert buffer to String
                String finalJson = buffer.toString();

                //Parse the first JSON object
                JSONObject parentObject = new JSONObject(finalJson);
                //Parse the second JSON object. I want the content in value, since that's where the
                //joke is.
                JSONObject valueObject = parentObject.getJSONObject("value");

                //Make a string of the JSON "value"-value.
                String joke = valueObject.getString("joke");

                //Not used anymore, but useful if you want the ID of the joke
                //int jokeNumber = valueObject.getInt("id");

                return joke;

                //Exception handling. Useful if anything would go wrong.
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }




        //At the end, we set the result as content of the TextView and it's shown.
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            jsonData.setText(result);
        }
    }




}
