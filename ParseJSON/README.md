# README #
Program Android App connected to the could

Develop an Android app containing the following elements:
1. Fetching items from the cloud. For example: weather, pictures, jokes. No limitation onwhich service to use as long as it is on a remote host.
2. Parsing the information in the response from the cloud. Depends on the format used by thecloud service. Typical formats are JSON or XML.
3. Performing the cloud request in background without freezing the main GUI.
4. Showing some kind of “Loading…” information while the cloud request is in progress.

1. I used Chuck Norris Database for retrieving data.
http://www.icndb.com/
2. Parsing is done with JSON
3. The main GUI is not freezing due to AsyncTask
4. There is a progressbar, but the information is retrieved and parsed so fast that you won't notice it.. (Have checked that the progresspar is displaying tho)

There is a .gif delivered at Fronter. The code is in the repository
https://bitbucket.org/Olemartin/android/src/3ab5848a94d51827541fb9287991e47ee5a098c9/ParseJSON/?at=master

* Please contact me if you have any questions at olemartinbratteberg@gmail.com