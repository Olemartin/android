package com.example.olema.listview;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

public class ListViewActivity extends Activity {
    //Initiate the list view and the content of it
    private ListView newList;
    private String[] pages = new String[] {"White snow", "Yellow snow"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Run the method when starting application/main activity
        viewList();
    }


    public void viewList(){
        //Choose the list that was created earlier
        newList = (ListView) findViewById(R.id.listView);
        //Set the content/data of the listView
        newList.setAdapter(new ArrayAdapter<String>(this, R.layout.content_main, pages));

        //Create a listener that waits until a button/list-item is clicked
        newList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            //Default setOnItemClickListener
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //If the first in the array is clicked, go to White activity
                if (position == 0){
                    //Intent is used here to start another activity or "screen".
                    Intent intent = new Intent(getApplicationContext(), White.class);
                    //Start the new activity
                    startActivity(intent);
                }
                //If the second in the array is clicked, go to White activity
                if (position == 1){
                    Intent intent = new Intent(getApplicationContext(), Yellow.class);
                    startActivity(intent);
                }
            }
        });
    }

}