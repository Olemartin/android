# README #
Program a simple Android app

The objective in this task is to develop a simple app containing these elements:
1. ListView showing a list of elements
2. Detail page showing details of a single item. You navigate here by clicking on an item in the ListView. Navigate back to ListView with back button
3. Layout files for the list page and details page

There is a .gif delivered at Fronter. The code is in the repository
https://bitbucket.org/Olemartin/android/src/28a4dc9b77186b958d0ff39329c74b0796bcf5b5/A2%20-%20ListView/?at=master

* Please contact me if you have any questions at olemartinbratteberg@gmail.com